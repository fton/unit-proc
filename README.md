[![Latest Release](https://gitlab.com/fton/unit-proc/-/badges/release.svg)](https://gitlab.com/fton/unit-proc/-/releases)
[![pipeline status](https://gitlab.com/fton/unit-proc/badges/main/pipeline.svg)](https://gitlab.com/fton/unit-proc/-/commits/main)
[![coverage report](https://gitlab.com/fton/unit-proc/badges/main/coverage.svg)](https://gitlab.com/fton/unit-proc/-/commits/main)
# Description
Procedural macro for generate [struct Unit] in compile time.

This package is the implementation of the procedural macro.
For use these macros in your code, [unitage] is convenient.

# Usage
```rust
extern crate typenum;

use unit_proc::unit;
use const_frac::Frac;
use physical_quantity:: { Unit, Dim };
use typenum;

let height = unit!("ft.").pq(5f64) + unit!("in.").pq(8f64);
let cm = unit!("cm").value(height);

assert_eq!(cm, 172.72);
```

# Project status
This package is in the very early stage.

[struct Unit]: https://docs.rs/physical-quantity/*/physical_quantity/unit/struct.Unit.html
[unitage]: https://docs.rs/unitage/*/unitage/