use unit_proc:: { unit, dim };
use physical_quantity:: { Dim, Unit, predefined::dim::* };
use typenum:: { self, Z0, P1, N1, N2 };
use const_frac::Frac;

fn main() {
    const U: Unit<f64, Force> = unit!(self, "kg m/s2", f64);

    assert_eq!(
        U,
        Unit {
            a: 1000.0,
            b: 0.0,
            dim: Dim::<P1, P1, N2, Z0, Z0, Z0, Z0>::new(),
        }
    );

    const V: Unit<f64, dim!(self, "ata")> = unit!(self, "ata", f64);

    assert_eq!(
        V,
        Unit {
            a: 98066500.0,
            b: 0.0,
            dim: Dim::<N1, P1, N2, Z0, Z0, Z0, Z0>::new(),
        }
    );
}