use unit_proc::unit;
use physical_quantity:: { Dim, Unit };
use typenum:: { self, Z0, P1, N2 };
use const_frac::Frac;

macro_rules! indirect {
    ($s: literal, $init: ident) => {
        unit!($crate, $s, $init)
    };
}

fn main() {
    let a = indirect!("kg m/s2", f64);

    assert_eq!(
        a,
        Unit {
            a: 1000.0,
            b: 0.0,
            dim: Dim::<P1, P1, N2, Z0, Z0, Z0, Z0>::new(),
        }
    );
}