use unit_proc::unit;
use const_frac::Frac;
use physical_quantity:: { Unit, Dim };
use typenum;

fn main() {
    let height = unit!("ft.").pq(5f64) + unit!("in.").pq(8f64);
    let cm = unit!("cm").value(height);

    assert_eq!(cm, 172.72);
}