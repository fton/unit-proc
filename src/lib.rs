#![doc = include_str!("../README.md")]
use proc_macro::TokenStream;
use syn:: { ExprPath, parse_macro_input };
use quote::quote;

mod unit;

use unit:: { TokenUnit, TokenDim };

#[proc_macro]
pub fn unit(input: TokenStream) -> TokenStream {
    let result = parse_macro_input!(input as TokenUnit);

    quote! { #result }.into()
}

#[proc_macro]
pub fn dim(input: TokenStream) -> TokenStream {
    let result = parse_macro_input!(input as TokenDim<ExprPath>);

    quote! { #result }.into()
}