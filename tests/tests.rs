#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("examples/unit.rs");
    t.pass("examples/indirect_unit.rs");
    t.pass("examples/indirect_init.rs");
    t.pass("examples/initializer.rs");
    t.pass("examples/dim.rs");
    //t.compile_fail("tests/04-variants-with-data.rs");
    //t.compile_fail("tests/05-match-expr.rs");
    //t.compile_fail("tests/06-pattern-path.rs");
    //t.compile_fail("tests/07-unrecognized-pattern.rs");
}